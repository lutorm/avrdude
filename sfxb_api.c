/*
 * avrdude - A Downloader/Uploader for AVR device programmers
 * Copyright (C) 2003-2004  Theodore A. Roth  <troth@openavr.org>
 * Copyright 2007 Joerg Wunsch <j@uriah.heep.sax.de>
 * Copyright 2008 Klaus Leidinger <klaus@mikrocontroller-projekte.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* $Id: sfxb.c,v 1.34 2008/07/25 21:14:43 joerg_wunsch Exp $ */

/*
 * avrdude interface for Atmel Low Cost Serial programmers which adher
 * to the protocol described in application note sfxb, sent to the end
 * device over an XBee link using API mode.
 */

#include "ac_cfg.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <assert.h>

#include "avrdude.h"
#include "avr.h"
#include "config.h"
#include "pgm.h"
#include "sfxb_api.h"
#include "serial.h"

#define SHADOW_SIZE 0x8000

/*
 * Private data for this programmer.
 */
struct pdata
{
    char shadow_mem[SHADOW_SIZE];

    /* 64-bit address of destination ZB device. */
    uint64_t dest_addr;
    /* 16-bit address of destination device. Stored in the byte order
       we get from the device. */
    uint16_t dest_short;
    uint16_t maxlen;
    uint8_t frame_id;
};

#define PDATA(pgm) ((struct pdata *)(pgm->cookie))

/* we cant' let the frame id become zero because them we wait for an
 * ack that never gets sent. */
void increment_frameid(PROGRAMMER * pgm)
{
    PDATA(pgm)->frame_id++;
    if (verbose >= 3)
        printf("Incremented frame id to %d\n",     PDATA(pgm)->frame_id);
    if(PDATA(pgm)->frame_id == 0)
        PDATA(pgm)->frame_id++;
}
    
/* assembles a tx frame from the specidifed data and address. The
 * length of the frame is returned. */
uint16_t assemble_tx_frame(uint8_t * data, int16_t len, PROGRAMMER * pgm,
                           uint8_t * frame)
{
    if (verbose >= 3)
      printf("Assembling TX frame to %llx with ID %d, len %d\n",PDATA(pgm)->dest_addr, PDATA(pgm)->frame_id, len);

    uint16_t fsd_len = len + 14;
    size_t i;

    frame[0] = 0x7e;
    frame[1] = (uint8_t)(fsd_len >> 8);
    frame[2] = (uint8_t)(fsd_len & 0x00ff);
    frame[3] = 0x10;
    frame[4] = PDATA(pgm)->frame_id;

    for(i=0; i<8; ++i)
        frame[5+i] = ((uint8_t*)&(PDATA(pgm)->dest_addr))[7-i];

    *(uint16_t*)(frame+13) = PDATA(pgm)->dest_short;
    frame[15] = 0x00;
    frame[16] = 0x00;
    memcpy(frame+17, data, len);
    
    uint8_t checksum = 0;
    for(i=0; i < fsd_len; ++i)
        checksum += frame[i+3];
    frame[fsd_len+3] = ((uint8_t)0xff) - checksum;
/*
    printf("TX frame:\n");
    for(i=0;i<fsd_len+4;++i)
        printf("%d\t%02x\t%c\n", (int)i, frame[i], frame[i]);
*/
    return fsd_len+4;
}

/* assembles a remote at command frame from the specified command,
 * optional parameter, and address. The length of the frame is
 * returned. */
uint16_t assemble_remote_at_frame(char* cmd, uint8_t * param, uint16_t paramlen,
                                  PROGRAMMER * pgm, uint8_t * frame)
{
    if (verbose >= 3)
      printf("Assembling remote AT frame to %llx with ID %d, param len %d\n",PDATA(pgm)->dest_addr, PDATA(pgm)->frame_id, paramlen);

    uint16_t fsd_len = paramlen + 15;
    size_t i;

    frame[0] = 0x7e;
    frame[1] = (uint8_t)(fsd_len >> 8);
    frame[2] = (uint8_t)(fsd_len & 0x00ff);
    frame[3] = 0x17;
    frame[4] = PDATA(pgm)->frame_id;

    for(i=0; i<8; ++i)
        frame[5+i] = ((uint8_t*)&(PDATA(pgm)->dest_addr))[7-i];

    *(uint16_t*)(frame+13) = PDATA(pgm)->dest_short;
    frame[15] = 0x02;

    frame[16] = cmd[0];
    frame[17] = cmd[1];

    if(paramlen>0)
        memcpy(frame+18, param, paramlen);

    uint8_t checksum = 0;
    for(i=0; i < fsd_len; ++i)
        checksum += frame[i+3];
    frame[fsd_len+3] = ((uint8_t)0xff) - checksum;
/*
    printf("Remote AT frame:\n");
    for(i=0;i<fsd_len+4;++i)
        printf("%d\t%02x\t%c\n", (int)i, frame[i], frame[i]);
*/
    return fsd_len+4;
}

size_t receive_frame(PROGRAMMER * pgm, uint8_t* frame, uint16_t* addr)
{
    if (verbose >= 3)
      printf("Receiving frame, cur frameid is %d\n", PDATA(pgm)->frame_id);
    size_t status;
    frame[0]=0;
    do {
        status = serial_recv(&pgm->fd, frame, 1);
	if (verbose >= 3)
	  printf("Looking for frame start, found 0x%02x\n", frame[0]);
    } while (frame[0] != 0x7e);

    // spooled up to a frame. receive size
    status = serial_recv(&pgm->fd, frame+1, 2);
    assert(!status);
    size_t fsd_len = frame[2] + 256*frame[1];
    status = serial_recv(&pgm->fd, frame+3, fsd_len+1);
    assert(!status);

    // check checksum
    size_t i;
    uint8_t checksum=0;
    for(i = 3; i<fsd_len+4; ++i)
        checksum += frame[i];

    if(checksum != 0xff)
    {
        fprintf(stderr, "ZB frame checksum failure\n");
        return 0;
    }

    uint8_t frame_type = frame[3];

    uint8_t addr_offset;
    switch (frame_type)
      {
      case 0x97: // remote command response
	addr_offset = 13;
	break;
      case 0x90: // receive packet
	addr_offset = 12;
	break;
      case 0x8b: // transmit status
	addr_offset = 5;
	break;
      default:
	printf("Unknown frameid\n");
	addr_offset = 0;
	break;
      };

    // flip bytes
    *addr = *((uint16_t*)(frame+addr_offset));
    
    if (verbose >= 3)
      printf("Received frame: type 0x%02x, len %d, from 0x%04hx\n", frame[3], fsd_len+4, *addr);
    
    return fsd_len+4;
}
    


static void sfxb_api_setup(PROGRAMMER * pgm)
{
    int i;

    if ((pgm->cookie = malloc(sizeof(struct pdata))) == 0) {
        fprintf(stderr,
                "%s: sfxb_api_setup(): Out of memory allocating private data\n",
                progname);
        exit(1);
    }
    memset(pgm->cookie, 0, sizeof(struct pdata));

    for (i=0; i<sizeof(PDATA(pgm)->shadow_mem); i++)
        PDATA(pgm)->shadow_mem[i] = 0xff;
}

static void sfxb_api_teardown(PROGRAMMER * pgm)
{
    free(pgm->cookie);
}

/* send function wraps buf in a API tx packet. */
static int sfxb_api_send(PROGRAMMER * pgm, char * buf, size_t len)
{
    uint8_t framebuf[200];
    size_t pos=0;
    uint16_t addr;

    while(pos<len)
    {
        // assemble frame and send it
        size_t thislen = len > PDATA(pgm)->maxlen ? PDATA(pgm)->maxlen : len;
        size_t send_len = 
            assemble_tx_frame((uint8_t*)buf+pos, thislen, pgm, framebuf);
        serial_send(&pgm->fd, framebuf, send_len);

        // check response
        do {
	  receive_frame(pgm, framebuf, &addr);
        } while (framebuf[3] != 0x8b && framebuf[4] != PDATA(pgm)->frame_id);
        if(framebuf[8]!=0x00) {
            fprintf(stderr, "TX response error: %x\n", framebuf[8]);
            increment_frameid(pgm);
            return -1;
        }
	if (verbose >= 2)
	  printf("Successfully sent %d bytes in frame %d\n",thislen,PDATA(pgm)->frame_id);

        increment_frameid(pgm);
        pos += thislen;
    }

    return 0;
}


static int sfxb_api_recv(PROGRAMMER * pgm, char * buf, size_t len)
{
    uint8_t framebuf[200];
    size_t nrecv=0;
    uint16_t addr;
    while(nrecv<len) {
        do {
	  receive_frame(pgm, framebuf, &addr);
        } while (framebuf[3] != 0x90 || addr != PDATA(pgm)->dest_short);

        // copy as much data as we want into buf
        size_t this_recv = framebuf[2]+255*framebuf[1];
        size_t i;
        for(i=0; i<this_recv && nrecv<len; ++i, nrecv++)
            buf[nrecv] = framebuf[15 + i];
    }
    return 0;
}


static int sfxb_api_drain(PROGRAMMER * pgm, int display)
{
  return serial_drain(&pgm->fd, display);
}


/*
 * issue the 'chip erase' command to the AVR device
 */
static int sfxb_api_chip_erase(PROGRAMMER * pgm, AVRPART * p)
{
    return 0;
}


static void sfxb_api_leave_prog_mode(PROGRAMMER * pgm)
{
    sfxb_api_send(pgm, ":S", 2); 
}


/*
 * issue the 'program enable' command to the AVR device
 */
static int sfxb_api_program_enable(PROGRAMMER * pgm, AVRPART * p)
{
    return 0;
}


/*
 * initialize the AVR device and prepare it to accept commands.
 * We assume that to reset the remote avr we need to cycle DIO3 on the dest Xbee.
 */
static int sfxb_api_initialize(PROGRAMMER * pgm, AVRPART * p)
{
    printf("Resetting device\n");
    sfxb_api_drain(pgm, 0);

    uint8_t framebuf[200];
    size_t framelen;
    uint16_t addr;

    uint8_t param = 0x04;
    framelen = assemble_remote_at_frame("D3", &param, 1, pgm, framebuf);
    serial_send(&pgm->fd, framebuf, framelen);
    do {
      receive_frame(pgm, framebuf, &addr);
    } while (framebuf[3] != 0x97 && framebuf[4]!=PDATA(pgm)->frame_id);
    if(framebuf[17]!=0x00) {
        fprintf(stderr, "Remote command response error: %x\n", framebuf[17]);
        return -1;
    }
    printf("Remote AT command acked successfully\n");
    increment_frameid(pgm);
    uint16_t addr_short = *(uint16_t*)(framebuf+13);
    if (addr_short != PDATA(pgm)->dest_short)
    {
      PDATA(pgm)->dest_short = addr_short;
      printf("Updating 16-bit destination address to %04x\n", PDATA(pgm)->dest_short);
    }

    // flush serial port while device is in reset
    sfxb_api_drain(pgm, 0);

    param = 0x05;
    framelen = assemble_remote_at_frame("D3", &param, 1, pgm, framebuf);
    serial_send(&pgm->fd, framebuf, framelen);
    do {
      receive_frame(pgm, framebuf, &addr);
    } while (framebuf[3] != 0x97 && framebuf[4] != PDATA(pgm)->frame_id);
    if(framebuf[17]!=0x00) {
        fprintf(stderr, "Remote command response error: %x\n", framebuf[17]);
        return -1;
    }
    printf("Remote AT command acked successfully\n");
    increment_frameid(pgm);

    /* the avr should now reset, so start looking for the magic 0x05. */
    printf("Looking for 0x05 from bootloader\n");
    char c;
    int i=100;
    while(i)
    {
        // wait for 0x05
        sfxb_api_recv(pgm, &c, 1);
        if (c == '\x05') {
	  // send 0x06
	  printf("Got 0x%02x from device - sending start command\n",c);
	  fflush(stdout);
	  sfxb_api_send(pgm, "\x06", 1);
	  break; //return 0;
        }
        printf("Got 0x%02x from device - fail\n",(uint8_t)c);
        fflush(stdout);
        --i;
    };

    if (!i) {
      return -1;
    }

    printf("Starting upload\n");
    fflush(stdout);

    return 0;
}


static void sfxb_api_disable(PROGRAMMER * pgm)
{
    /* Do nothing. */
    return;
}


static void sfxb_api_enable(PROGRAMMER * pgm)
{
    /* Do nothing. */
    return;
}


/*
 * transmit an AVR device command and return the results; 'cmd' and
 * 'res' must point to at least a 4 byte data buffer
 */
static int sfxb_api_cmd(PROGRAMMER * pgm, unsigned char cmd[4], 
                      unsigned char res[4])
{
    return -1;
}


static int sfxb_api_open(PROGRAMMER * pgm, char * port)
{
    /*
    *  If baudrate was not specified use 19.200 Baud
    */
    if(pgm->baudrate == 0) {
        pgm->baudrate = 19200;
    }

    strcpy(pgm->port, port);
    serial_open(port, pgm->baudrate, &pgm->fd);

    /*
    * drain any extraneous input
    */
    sfxb_api_drain (pgm, 0);
        
    return 0;
}

static void sfxb_api_close(PROGRAMMER * pgm)
{
    sfxb_api_leave_prog_mode(pgm);
    serial_close(&pgm->fd);
    pgm->fd.ifd = -1;
}


static void sfxb_api_display(PROGRAMMER * pgm, const char * p)
{
    return;
}


/* Signature byte reads are always 3 bytes. */

static int sfxb_api_read_sig_bytes(PROGRAMMER * pgm, AVRPART * p, AVRMEM * m)
{
    if (m->size < 3) {
        fprintf(stderr, "%s: memsize too small for sig byte read", progname);
        return -1;
    }

    // fake a mega328p
    m->buf[0] = 0x1E;
    m->buf[1] = 0x95;
    m->buf[2] = 0x0F;

    return 3;
}


static int sfxb_api_paged_load (PROGRAMMER * pgm, AVRPART * p, AVRMEM* m,
				    int page_size, int n_bytes )
{
    int i;
    
    for (i=0; i<n_bytes; i++)
        m->buf[i] = PDATA(pgm)->shadow_mem[i];
    report_progress (n_bytes, n_bytes, NULL);
    return n_bytes;
}


static int sfxb_api_paged_write(PROGRAMMER * pgm, AVRPART * p, AVRMEM * m,
                              int page_size, int n_bytes)
{
    int rval = 0;
    unsigned int curMemAdr = 0;
    char c;
    char cmd[5 + 128];
    int i;
    char adrL, adrH, checksum;
    unsigned int pageSize = 0;
    
    if (verbose >= 2)
        printf("sfxb_api_paged_write: n_bytes 0x%02x\n", n_bytes);
    
    while (1) {
        sfxb_api_recv(pgm, &c, 1);
        if (c == 'T')
        {
            printf("Remote device acks page %04x received successfully.\n", curMemAdr-pageSize);
            ; // all is well
        }
        else if (c == '\x07')
        {
            curMemAdr -= pageSize;
            printf("Remote device requests retransmit of page %04x.\n", curMemAdr);
        }
	else if (c == '\x05')
	  {
	    // leftover 0x05?
	    printf("Got errant 0x05 -- ignoring\n");
	    continue;
	  }
        else {
	  fprintf(stderr, "    Error : Incorrect response %x from target IC. Programming is incomplete and will now halt.", c);
            rval = -1;  // die
            break;
        }
        
        if (curMemAdr >= n_bytes)
            break;

        // convert curMemAdr to 2 bytes (adrh, adrL)
        adrL = curMemAdr & 0xff;
        adrH = curMemAdr >> 8;
        pageSize = (n_bytes - curMemAdr >= 128) ? 128 : n_bytes - curMemAdr;
    	if (verbose >= 2)
            printf("\nsfxb_api_paged_write: curMemAdr 0x%04x, 0x%02x\n", curMemAdr, pageSize);
        // calc checksum = 256 - (pageSize + adrH + adrL + data[..])
        checksum = pageSize + adrH + adrL;
        for (i=0; i<pageSize; i++)
            checksum += m->buf[curMemAdr + i];
        checksum = 256 - checksum;
        // send ':'
        cmd[0] = ':';
        // send pageSize, adrL, adrH, checksum
        cmd[1] = pageSize;
        cmd[2] = adrL;
        cmd[3] = adrH;
        cmd[4] = checksum;
        // send data[curMemAdr to curMemAdr + pageSize]
        for (i=0; i<pageSize; i++) {
            cmd[5 + i] = m->buf[curMemAdr + i];
            PDATA(pgm)->shadow_mem[curMemAdr + i] = m->buf[curMemAdr + i];
            assert(curMemAdr+i < SHADOW_SIZE);
        }
        sfxb_api_send(pgm, cmd, 5 + pageSize);

        curMemAdr += pageSize;
        report_progress (curMemAdr, n_bytes, NULL);
    }
    rval = curMemAdr;

    return rval;
}

static int  sfxb_api_read_byte (struct programmer_t * pgm, AVRPART * p, AVRMEM * mem,
			   unsigned long addr, unsigned char * value)
{
    if ((strcmp(mem->desc, "eeprom") == 0) && (addr >= 0x1fc) && (addr <= 0x1ff)) {
        // fake cycle count read
        *value = 0xff;
        return 0;
    }
    // fail for anything else
    return -1;
}



static int  sfxb_api_write_byte (struct programmer_t * pgm, AVRPART * p, AVRMEM * mem,
			   unsigned long addr, unsigned char data)
{
    return -1;
}


static int sfxb_api_parseextparms(PROGRAMMER * pgm, LISTID extparms)
{
  LNODEID ln;
  const char *extended_param;
  int rv = 0;

  for (ln = lfirst(extparms); ln; ln = lnext(ln)) {
    extended_param = ldata(ln);

    if (strncmp(extended_param, "dest=", strlen("dest=")) == 0) {
        uint64_t dest;
        if (sscanf(extended_param, "dest=%llx", &dest) != 1) {
            fprintf(stderr,
                    "%s: sfxb_api_parseextparms(): invalid devcode '%s'\n",
                    progname, extended_param);
            rv = -1;
            continue;
        }
        if (verbose >= 2) {
            fprintf(stderr,
                    "%s: sfxb_api_parseextparms(): using destination 0x%llx\n",
                    progname, dest);
        }
        PDATA(pgm)->dest_addr = dest;

      continue;
    }

    fprintf(stderr,
            "%s: sfxb_api_parseextparms(): invalid extended parameter '%s'\n",
            progname, extended_param);
    rv = -1;
  }

  PDATA(pgm)->maxlen = 240;
  PDATA(pgm)->frame_id = 1;
  PDATA(pgm)->dest_short = 0xfeff;

  printf("%s: sfxb_api using destination 0x%llx\n", progname, PDATA(pgm)->dest_addr);

  return rv;
}



void sfxb_api_initpgm(PROGRAMMER * pgm)
{
    strcpy(pgm->type, "sfxb_api");

    /*
    * mandatory functions
    */

    pgm->initialize     = sfxb_api_initialize;
    pgm->display        = sfxb_api_display;
    pgm->enable         = sfxb_api_enable;
    pgm->disable        = sfxb_api_disable;
    pgm->program_enable = sfxb_api_program_enable;
    pgm->chip_erase     = sfxb_api_chip_erase;
    pgm->cmd            = sfxb_api_cmd;
    pgm->open           = sfxb_api_open;
    pgm->close          = sfxb_api_close;
    pgm->read_byte      = sfxb_api_read_byte;
    pgm->write_byte     = sfxb_api_write_byte;

    /*
    * optional functions
    */

    pgm->paged_load     = sfxb_api_paged_load;
    pgm->paged_write    = sfxb_api_paged_write;
    pgm->read_sig_bytes = sfxb_api_read_sig_bytes;
    pgm->setup          = sfxb_api_setup;
    pgm->teardown       = sfxb_api_teardown;
    pgm->parseextparams = sfxb_api_parseextparms;

}
