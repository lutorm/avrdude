/*
 * avrdude - A Downloader/Uploader for AVR device programmers
 * Copyright (C) 2003-2004  Theodore A. Roth  <troth@openavr.org>
 * Copyright 2007 Joerg Wunsch <j@uriah.heep.sax.de>
 * Copyright 2008 Klaus Leidinger <klaus@mikrocontroller-projekte.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* $Id: sfxb.c,v 1.34 2008/07/25 21:14:43 joerg_wunsch Exp $ */

/*
 * avrdude interface for Atmel Low Cost Serial programmers which adher to the
 * protocol described in application note sfxb.
 */

#include "ac_cfg.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>

#include "avrdude.h"
#include "avr.h"
#include "config.h"
#include "pgm.h"
#include "sfxb.h"
#include "serial.h"

#define SHADOW_SIZE 0x8000

/*
 * Private data for this programmer.
 */
struct pdata
{
    char shadow_mem[SHADOW_SIZE];

    char cmd_port[100];
    char reset_cmd_channel[10];
};

#define PDATA(pgm) ((struct pdata *)(pgm->cookie))

static void sfxb_setup(PROGRAMMER * pgm)
{
    int i;

    if ((pgm->cookie = malloc(sizeof(struct pdata))) == 0) {
        fprintf(stderr,
                "%s: sfxb_setup(): Out of memory allocating private data\n",
                progname);
        exit(1);
    }
    memset(pgm->cookie, 0, sizeof(struct pdata));

    for (i=0; i<sizeof(PDATA(pgm)->shadow_mem); i++)
        PDATA(pgm)->shadow_mem[i] = 0xff;
}

static void sfxb_teardown(PROGRAMMER * pgm)
{
    free(pgm->cookie);
}


static int sfxb_send(PROGRAMMER * pgm, char * buf, size_t len)
{
    return serial_send(&pgm->fd, (unsigned char *)buf, len);
}


static int sfxb_recv(PROGRAMMER * pgm, char * buf, size_t len)
{
  int rv=-1;
  int retries=10;
  while (rv<0 && retries-->0)
    {
      rv = serial_recv(&pgm->fd, (unsigned char *)buf, len);
    }
  
  if (rv < 0) {
    fprintf(stderr,
	    "%s: sfxb_recv(): programmer is not responding\n",
	    progname);
    exit(1);
  }
  return 0;
}


static int sfxb_drain(PROGRAMMER * pgm, int display)
{
  return serial_drain(&pgm->fd, display);
}


/*
 * issue the 'chip erase' command to the AVR device
 */
static int sfxb_chip_erase(PROGRAMMER * pgm, AVRPART * p)
{
    return 0;
}


static void sfxb_leave_prog_mode(PROGRAMMER * pgm)
{
    sfxb_send(pgm, ":S", 2);

    // if we do nothing here, it appears these bytes can not get sent
    // and the device hangs up. attempt to read something back to make
    // it wait a bit.
    char c;
    sfxb_recv(pgm, &c, 1);
}


/*
 * issue the 'program enable' command to the AVR device
 */
static int sfxb_program_enable(PROGRAMMER * pgm, AVRPART * p)
{
    return 0;
}


/*
 * initialize the AVR device and prepare it to accept commands
 */
static int sfxb_initialize(PROGRAMMER * pgm, AVRPART * p)
{
    char * cmd_port = PDATA(pgm)->cmd_port;
    char * reset_cmd_channel = PDATA(pgm)->reset_cmd_channel;
    if (strlen(cmd_port))
    {
        // we have a command port - open and send reset
        union filedescriptor cmd_fd;
        char resetcmd[100];
        snprintf(resetcmd, 100, "%s \x04\n%s \x05\n", reset_cmd_channel, reset_cmd_channel);
        if (verbose > 1)
            printf("Using command channel to send reset command: %s\n", resetcmd);
        serial_open(cmd_port, 19200, &cmd_fd);
        serial_send(&cmd_fd, (unsigned char*)resetcmd, strlen(resetcmd));
        serial_close(&cmd_fd);
    }

    if (verbose > 1)
        printf("Waiting for 0x05 from device\n");
    
    char c;
    // wait for 0x05
    sfxb_recv(pgm, &c, 1);
    if (c == '\x05') {
        // send 0x06
        sfxb_send(pgm, "\x06", 1);
        if (verbose > 1)
            printf("Got 0x05 from device - starting upload\n");
        return 0;
    }
    printf("Got '%c' from device - fail\n",c);
    return -1;

}


static void sfxb_disable(PROGRAMMER * pgm)
{
    /* Do nothing. */
    return;
}


static void sfxb_enable(PROGRAMMER * pgm)
{
    /* Do nothing. */
    return;
}


/*
 * transmit an AVR device command and return the results; 'cmd' and
 * 'res' must point to at least a 4 byte data buffer
 */
static int sfxb_cmd(PROGRAMMER * pgm, unsigned char cmd[4], 
                      unsigned char res[4])
{
    return -1;
}


static int sfxb_open(PROGRAMMER * pgm, char * port)
{
    /*
    *  If baudrate was not specified use 19.200 Baud
    */
    if(pgm->baudrate == 0) {
        pgm->baudrate = 19200;
    }

    strcpy(pgm->port, port);
    serial_open(port, pgm->baudrate, &pgm->fd);

    /*
    * drain any extraneous input
    */
    sfxb_drain (pgm, 0);
        
    return 0;
}

static void sfxb_close(PROGRAMMER * pgm)
{
    sfxb_leave_prog_mode(pgm);
    serial_close(&pgm->fd);
    pgm->fd.ifd = -1;
}


static void sfxb_display(PROGRAMMER * pgm, const char * p)
{
    return;
}


/* Signature byte reads are always 3 bytes. */

static int sfxb_read_sig_bytes(PROGRAMMER * pgm, AVRPART * p, AVRMEM * m)
{
    if (m->size < 3) {
        fprintf(stderr, "%s: memsize too small for sig byte read", progname);
        return -1;
    }

    // fake a mega328p
    m->buf[0] = 0x1E;
    m->buf[1] = 0x95;
    m->buf[2] = 0x0F;

    return 3;
}


static int sfxb_paged_load (PROGRAMMER * pgm, AVRPART * p, AVRMEM* m,
				    int page_size, int n_bytes )
{
    int i;
    
    for (i=0; i<n_bytes; i++)
        m->buf[i] = PDATA(pgm)->shadow_mem[i];
    report_progress (n_bytes, n_bytes, NULL);
    return n_bytes;
}


static int sfxb_paged_write(PROGRAMMER * pgm, AVRPART * p, AVRMEM * m,
                              int page_size, int n_bytes)
{
    int rval = 0;
    unsigned int curMemAdr = 0;
    char c;
    char cmd[5 + 128];
    int i;
    char adrL, adrH, checksum;
    unsigned int pageSize = 0;
    
    if (verbose > 3)
        printf("sfxb_paged_write: n_bytes %x\n", n_bytes);
    
    while (1) {
        sfxb_recv(pgm, &c, 1);
        if (c == 'T')
        {
            printf("Remote device acks page %04x received successfully.\n", curMemAdr-pageSize);
            ; // all is well
        }
        else if (c == '\x07')
        {
            curMemAdr -= pageSize;
            printf("Remote device requests retransmit of page %04x.\n", curMemAdr);
        }
	else if (c == '\x05')
	  {
	    // leftover 0x05?
	    printf("Got errant 0x05 -- ignoring\n");
	    continue;
	  }
        else {
            fprintf(stderr, "    Error : Incorrect response from target IC. Programming is incomplete and will now halt.n");
            rval = -1;  // die
            break;
        }
        
        if (curMemAdr >= n_bytes)
            break;

        // convert curMemAdr to 2 bytes (adrh, adrL)
        adrL = curMemAdr & 0xff;
        adrH = curMemAdr >> 8;
        pageSize = (n_bytes - curMemAdr >= 128) ? 128 : n_bytes - curMemAdr;
    	if (verbose > 3)
            printf("\nsfxb_paged_write: curMemAdr %x, %x\n", curMemAdr, pageSize);
        // calc checksum = 256 - (pageSize + adrH + adrL + data[..])
        checksum = pageSize + adrH + adrL;
        for (i=0; i<pageSize; i++)
            checksum += m->buf[curMemAdr + i];
        checksum = 256 - checksum;
        // send ':'
        cmd[0] = ':';
        // send pageSize, adrL, adrH, checksum
        cmd[1] = pageSize;
        cmd[2] = adrL;
        cmd[3] = adrH;
        cmd[4] = checksum;
        // send data[curMemAdr to curMemAdr + pageSize]
        for (i=0; i<pageSize; i++) {
            cmd[5 + i] = m->buf[curMemAdr + i];
            PDATA(pgm)->shadow_mem[curMemAdr + i] = m->buf[curMemAdr + i];
            assert(curMemAdr+i < SHADOW_SIZE);
        }
        sfxb_send(pgm, cmd, 5 + pageSize);

        curMemAdr += pageSize;
        report_progress (curMemAdr, n_bytes, NULL);
    }
    rval = curMemAdr;

    return rval;
}

static int  sfxb_read_byte (struct programmer_t * pgm, AVRPART * p, AVRMEM * mem,
			   unsigned long addr, unsigned char * value)
{
    if ((strcmp(mem->desc, "eeprom") == 0) && (addr >= 0x1fc) && (addr <= 0x1ff)) {
        // fake cycle count read
        *value = 0xff;
        return 0;
    }
    // fail for anything else
    return -1;
}



static int  sfxb_write_byte (struct programmer_t * pgm, AVRPART * p, AVRMEM * mem,
			   unsigned long addr, unsigned char data)
{
    return -1;
}


static int sfxb_parseextparms(PROGRAMMER * pgm, LISTID extparms)
{
  LNODEID ln;
  const char *extended_param;
  int rv = 0;

  PDATA(pgm)->cmd_port[0]='\x00';
  sprintf(PDATA(pgm)->reset_cmd_channel, "D3");

  for (ln = lfirst(extparms); ln; ln = lnext(ln)) {
    extended_param = ldata(ln);

    if (strncmp(extended_param, "cmd_port=", strlen("cmd_port=")) == 0) 
    {
        if (sscanf(extended_param, "cmd_port=%s", PDATA(pgm)->cmd_port) != 1) {
            fprintf(stderr,
                    "%s: sfxb_parseextparms(): invalid devcode '%s'\n",
                    progname, extended_param);
            rv = -1;
            continue;
        }
        continue;
    }

    if (strncmp(extended_param, "reset_cmd_channel=", strlen("reset_cmd_channel")) == 0) 
    {
        if (sscanf(extended_param, "reset_cmd_channel=%s", PDATA(pgm)->reset_cmd_channel) != 1) {
            fprintf(stderr,
                    "%s: sfxb_parseextparms(): invalid devcode '%s'\n",
                    progname, extended_param);
            rv = -1;
            continue;
        }
        continue;
    }

    fprintf(stderr,
            "%s: sfxb_parseextparms(): invalid extended parameter '%s'\n",
            progname, extended_param);
    rv = -1;
  }

  printf("%s: sfxb using reset channel '%s' on cmd_port '%s'\n", progname, PDATA(pgm)->reset_cmd_channel,PDATA(pgm)->cmd_port);

  return rv;
}


void sfxb_initpgm(PROGRAMMER * pgm)
{
    strcpy(pgm->type, "sfxb");

    /*
    * mandatory functions
    */

    pgm->initialize     = sfxb_initialize;
    pgm->display        = sfxb_display;
    pgm->enable         = sfxb_enable;
    pgm->disable        = sfxb_disable;
    pgm->program_enable = sfxb_program_enable;
    pgm->chip_erase     = sfxb_chip_erase;
    pgm->cmd            = sfxb_cmd;
    pgm->open           = sfxb_open;
    pgm->close          = sfxb_close;
    pgm->read_byte      = sfxb_read_byte;
    pgm->write_byte     = sfxb_write_byte;

    /*
    * optional functions
    */

    pgm->paged_load     = sfxb_paged_load;
    pgm->paged_write    = sfxb_paged_write;
    pgm->read_sig_bytes = sfxb_read_sig_bytes;
    pgm->setup          = sfxb_setup;
    pgm->teardown       = sfxb_teardown;
    pgm->parseextparams = sfxb_parseextparms;
}
